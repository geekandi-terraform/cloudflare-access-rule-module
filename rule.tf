# whitelist an IP
resource "cloudflare_access_rule" "whitelisted_ip" {
  count = length(var.whitelist_ips)
  notes = var.notes
  mode  = "whitelist"

  configuration {
    target = "ip_range"
    value  = element(var.whitelist_ips, count.index)
  }
}

# blacklist an IP
resource "cloudflare_access_rule" "blacklisted_ip" {
  count = length(var.blacklist_ips)
  notes = var.notes
  mode  = "block"

  configuration {
    target = "ip_range"
    value  = element(var.blacklist_ips, count.index)
  }
}
