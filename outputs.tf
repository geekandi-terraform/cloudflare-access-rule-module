output "whitelist_id" {
  value = [cloudflare_access_rule.whitelisted_ip.*.id]
}

output "blacklist_id" {
  value = [cloudflare_access_rule.blacklisted_ip.*.id]
}

# does not work
# output "whitelist_zone_id" {
#   value = ["${cloudflare_access_rule.whitelisted_ip.*.zone_id}"]
# }
