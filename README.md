# Cloudflare Access Rule Module

[Terraform cloudflare_access_rule](https://www.terraform.io/docs/providers/cloudflare/r/access_rule.html)

> **Requires Terraform 0.12 or higher**

This module will create rules in Cloudflare.

Suggestion: you should use durable state storage.

## usage example
```
module "cloudflare_whitelist" {
  source        = "git::https://grot.geeks.org/tf/cloudflare-access-rule-module.git"

  notes         = "Whitelisted IPs"
  whitelist_ips = [
    "192.168.241.0/24",
    "192.168.242.0/24"
  ]
}

module "cloudflare_blacklist" {
  source        = "git::https://grot.geeks.org/tf/cloudflare-access-rule-module.git"

  notes         = "Blacklisted IPs"
  whitelist_ips = [
    "10.22.0.0/16",
    "10.24.0.0/24"
  ]
}
```

## required variables
N/A

## optional variables
* `notes` - a comment for the entry
* `whitelist_ips` - list of IPs to whitelist using Terraform list format
* `blacklist_ips` - list of IPs to whitelist using Terraform list format

## outputs
* `whitelist_id` - The access rule ID list for whitelisted IPs
* `blacklist_id` - The access rule ID list for blacklisted IPs

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_cloudflare"></a> [cloudflare](#requirement\_cloudflare) | ~> 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_cloudflare"></a> [cloudflare](#provider\_cloudflare) | 3.7.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [cloudflare_access_rule.blacklisted_ip](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/access_rule) | resource |
| [cloudflare_access_rule.whitelisted_ip](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/access_rule) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_blacklist_ips"></a> [blacklist\_ips](#input\_blacklist\_ips) | n/a | `list(string)` | `[]` | no |
| <a name="input_configuration_target"></a> [configuration\_target](#input\_configuration\_target) | n/a | `string` | `""` | no |
| <a name="input_configuration_value"></a> [configuration\_value](#input\_configuration\_value) | n/a | `string` | `""` | no |
| <a name="input_mode"></a> [mode](#input\_mode) | n/a | `string` | `""` | no |
| <a name="input_notes"></a> [notes](#input\_notes) | n/a | `string` | `""` | no |
| <a name="input_whitelist_ips"></a> [whitelist\_ips](#input\_whitelist\_ips) | n/a | `list(string)` | `[]` | no |
| <a name="input_zone"></a> [zone](#input\_zone) | n/a | `string` | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_blacklist_id"></a> [blacklist\_id](#output\_blacklist\_id) | n/a |
| <a name="output_whitelist_id"></a> [whitelist\_id](#output\_whitelist\_id) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
