variable "notes" {
  type    = string
  default = ""
}

variable "mode" {
  type    = string
  default = ""
}

variable "configuration_target" {
  type    = string
  default = ""
}

variable "configuration_value" {
  type    = string
  default = ""
}

variable "zone" {
  type    = string
  default = ""
}

variable "whitelist_ips" {
  type    = list(string)
  default = []
}

variable "blacklist_ips" {
  type    = list(string)
  default = []
}
